<!--
SPDX-FileCopyrightText: 2023 ilja@ilja.space

SPDX-License-Identifier: AGPL-3.0-only
-->

# Music Ilja Space

This runs on <https://music.ilja.space>. It's a search form which looks for music on [Jamendo](https://jamendo.com) and plays it in a [Webamp player](https://webamp.org/).

## Set up for production

```sh
# Get the source
git clone codeberg.org/ilja/music_ilja_space
cd music_ilja_space

# Download js dependencies
mkdir -p 'static/deps'
curl 'https://unpkg.com/webamp@1.4.0/built/webamp.bundle.min.js' -o 'static/deps/webamp.bundle.min.js'
curl 'https://unpkg.com/butterchurn@2.6.7/lib/butterchurn.min.js' -o 'static/deps/butterchurn.min.js'
curl 'https://unpkg.com/butterchurn-presets@2.4.7/lib/butterchurnPresets.min.js' -o 'static/deps/butterchurnPresets.min.js'

# Set the config files
cp config/config.js.example config/config.js

# Replace the placeholder `your_client_id` with the client_id you got at <https://developer.jamendo.com/>
nano config/config.js
```

You can open the `index.html` file directly in a browser, or you can point to it using a reverse proxy like Caddy or NGINX.

## What the future *may* bring

There's some extra stuff which may be cool to have. I'll add them when I feel like it and depending on how much I personally want them.

* I like an option to add the client_id via the reverse proxy. For some reason it doesn't work with nginx :'( I haven't tried with Caddy.
* [x] Add Milkdrop visualisations with [butterchurn](https://github.com/jberg/butterchurn)
* Add/change skin
* Search/present with other focus than tracks (e.g. albums, artists...)
* Make it look better
  * See Frutiger Aero
    * <https://aesthetics.fandom.com/wiki/Frutiger_Aero>
    * <https://frutiger-aero.neocities.org/tutorials>
* Better presentation of results (different look, have link to source/Jamendo page...)
* Have playlists
    * These could be from Jamendo
    * Or stored in the browser and allow to import/export
    * I don't want to start doing my own usermanagement, but login to e.g. Nextcloud could maybe be an option if not too hard
* Download songs, albums...
* Idealy, it should always be possible to run by openening the index.html, and it should never require a back-end
* ...

## Contributing

Feel free to hack on this. PR's are welcome, but I don't want to go much beyond the limitations I added to the list above.

Most code is located in the `index.html` file directly. Extra fetches are only done for config and dependencies.

Static files can be found in *music_ilja_space/static/*.

The snippets folder contains example snippets used in the code, but should never be required by the `index.html` file to be present.

## Credits

* Webamp <https://github.com/captbaritone/webamp/blob/master/packages/webamp/docs/usage.md>
* Music is from [Jamendo](https://jamendo.com), all is released under some [Creative Commons](https://creativecommons.org/)

## License

    A simple site that can search music on Jamendo and has a webamp music player embeded
    Copyright (C) 2023  ilja@ilja.space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
